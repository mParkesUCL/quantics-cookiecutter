Quantics Calculations
====================

A cookie cutter for Quantics calculations on a given molecule

Requirements
------------
Install `cookiecutter` command line: `pip install cookiecutter`   

or if using conda

command line: `conda install -c conda forge cookiecutter`

Usage
-----
To start a new quantics project:

`cookiecutter <local folder>`

or

`cookiecutter gl:mParkesUCL/quantics-cookiecutter`

Project Structure
-----------------

```
    ├── AUTHORS.md
    ├── LICENSE
    ├── README.md
    ├── calculations
    │   ├── ab initio
    │   ├── quantics_dynamic
    │   ├── quantics_static
	│   │	├──	inputs
	│   │	├──	operators
	│   │	└──	outputs
    │   └── vcham
	│    	├──	vchfit
	│    	├──	vcpnts
	│    	└──	vctrans
    ├── config	
    ├── literature
    ├── notebooks
    ├── reports
    └── └── figures

```


License
-------
This project is licensed under the terms of the [BSD License](/LICENSE)
