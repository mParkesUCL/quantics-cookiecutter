{{cookiecutter.project_slug}}
==============================

{{cookiecutter.project_short_description}}

Project Organization
--------------------

    .
    ├── AUTHORS.md
    ├── LICENSE
    ├── README.md
    ├── calculations
    │   ├── ab initio
    │   ├── quantics_dynamic
    │   ├── quantics_static
	│   │	├──	inputs
	│   │	├──	operators
	│   │	└──	outputs
    │   └── vcham
	│    	├──	vchfit
	│    	├──	vcpnts
	│    	└──	vctrans
    ├── config	
    ├── literature
    ├── notebooks
    └──	reports
        └── figures

